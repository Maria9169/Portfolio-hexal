var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync').create();
var csso = require('gulp-csso');

gulp.task('sass', function(){
    gulp.src('style.scss')
        .pipe(sass()) // Using gulp-sass
        .pipe(csso())
        .pipe(gulp.dest('dist'))
});
gulp.task('watch', function(){
    gulp.watch('style.scss', ['sass']);
});

gulp.task('default', ['watch' ]);

gulp.task('browser-sync', function() {
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });
});
